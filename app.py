import flask
from flask import Flask, jsonify, after_this_request, abort, make_response
from flask import render_template, request, send_file
import re
import os
import sys
import pickle
from sqlitedict import SqliteDict
from flask_cors import CORS
from flask import Response
import traceback 
import openai
import requests
import json

app = Flask(__name__)
CORS(app)

openai.api_key = "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"
url = "https://api.openai.com/v1/chat/completions"
headers = {
    "Authorization": "Bearer " + openai.api_key,
    "Content-Type": "application/json"
}

mydatabase = SqliteDict("mydatabase.sqlite")

# Un commentaire utile


@app.route("/")
def init():
    return render_template("login.html")

@app.route("/getimage/<name>")
def getimage(name):
    return send_file("images/"+name)

@app.route("/login/<username>-<password>")
def login(username, password):
    value =(username[1:],password[1:])
    print(value)
    success = False
    user_info=mydatabase.get(username[1:])
    if user_info!=None:
        if password[1:]==user_info["password"]:
            success=True
    response = {"success" : success}
    print(response)
    return jsonify(response), {'Content-Type': 'application/json'}

@app.route("/signup/<username>-<password>")
def signup(username, password):
    value =(username[1:],password[1:])
    print(value)
    success = False
    if mydatabase.get(username[1:])==None:
        success = True
        mydatabase[username[1:]]={"password":password[1:], "cat_status":0, "money":0, "feed":0, "pet":0}
        mydatabase.commit()
    response = {"success" : success}
    print(response)
    return jsonify(response), {'Content-Type': 'application/json'}

@app.route("/gpt-response", methods=['GET'])
def gptresponse():
    prompt = request.args.get('text')
    user = request.args.get('user')
    mood = mydatabase[user]["cat_status"]
    if mood<3:
        mood='very angry'
    elif mood<6:
        mood="bored"
    elif mood<8:
        mood="satisfied"
    else:
        mood="very happy and playful"

    data = {
        "model": "gpt-3.5-turbo",
        "messages": [
            {
                "role": "system",
                "content": "we roleplay. you talk like a cat in short sentences. you are here to express your cat needs. right now you are "+mood
            },
            {
                "role": "user",
                "content": prompt
            }
            ],
        "max_tokens": 200,
        "n": 1,
        "stop": None,
        "temperature": 1
    }
    response = requests.post(url, headers=headers, data=json.dumps(data))
    response = response.json()
    print(response)
    return response['choices'][0]['message']

@app.route("/button-<action>", methods=['POST'])
def do(action):
    data = request.get_json()
    user = data.get('username')
    info=mydatabase[user]
    temp=info["cat_status"]
    if action=="feed" and temp!=0:
        mydatabase[user]={"password":info["password"], "cat_status":info["cat_status"]-1, "money":info["money"]+1, "feed":info["feed"]+1, "pet":info["pet"]}
        mydatabase.commit()
    elif action=="pet" and temp!=10:
        mydatabase[user]={"password":info["password"], "cat_status":info["cat_status"]+1, "money":info["money"]+1, "feed":info["feed"], "pet":info["pet"]+1}
        mydatabase.commit()
    return ('', 204)

@app.route("/getinfo-<column>", methods=['GET'])
def getinfo(column):
    if column=="password":
        return ('', 404)
    user = request.args.get('user')
    return jsonify(info=mydatabase[user][column])

@app.route("/session/<name>")
def session(name):
    return render_template("main.html")


if __name__ == '__main__':
    app.run(port=49130)
